<?php

use Illuminate\Database\Seeder;
use App\Entities\Role;
use App\Entities\Permission;

class SeedRolesAndPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Reset cached roles and permissions
      app()['cache']->forget('spatie.permission.cache');

      // create permissions
      Permission::create(['name' => 'administer roles & permissions']);
      Permission::create(['name' => 'manage users']);
      Permission::create(['name' => 'manage types']);
      Permission::create(['name' => 'manage threads']);

      // create roles and assign created permissions
      $role = Role::create(['name' => 'admin']);
      $role->givePermissionTo(Permission::all());
      
    }
}
