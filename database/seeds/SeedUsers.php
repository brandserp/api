<?php

use Illuminate\Database\Seeder;
use App\Entities\User;
use App\Events\UserCreated;
use Illuminate\Support\Facades\Hash;

class SeedUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userInfo = [
          "first_name" => "Tommy",
          "last_name" => "Cresine",
          "email" => "tcresine@gmail.com",
          "password" => Hash::make("murakami")
        ];
        $user = User::create($userInfo);
        $user->assignRole('admin');

        $userInfo1 = [
          "first_name" => "Marc",
          "last_name" => "Brands",
          "email" => "test@brandscapital.com",
          "password" => Hash::make("password")
        ];
        $user1 = User::create($userInfo1);
        $user1->assignRole('admin');

        $userInfo2 = [
          "first_name" => "Annie",
          "last_name" => "Bai",
          "email" => "hewanginc@gmail.com",
          "password" => Hash::make("password")
        ];

        $user2 = User::create($userInfo2);
        $user2->assignRole('admin');

        $userInfo3 = [
          "first_name" => "Benjamin",
          "last_name" => "Smalley",
          "email" => "fashomixer@yahoo.com",
          "password" => Hash::make("password")
        ];

        $user3 = User::create($userInfo3);
        $user3->assignRole('admin');

        if (App::environment('stage')) {
            event(new UserCreated($user));
            event(new UserCreated($user1));
            event(new UserCreated($user2));
            event(new UserCreated($user3));
        }
    }
}
