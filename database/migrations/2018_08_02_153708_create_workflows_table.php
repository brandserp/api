<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateWorkflowsTable.
 */
class CreateWorkflowsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('workflows', function(Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->text('bpmn_xml');
			$table->string('bpmn_id');
            $table->string('deployment_id')->nullable();
			$table->boolean('published')->default(false);
            $table->integer('document_type_id');
            $table->json('options')->default(json_encode([
                "startInstanceOnUpload" => false
            ]));
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('workflows');
	}
}
