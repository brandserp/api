<?php

use Dingo\Api\Routing\Router;
use Illuminate\Support\Facades\Broadcast;
use Unisharp\S3\Presigned\Facades\S3Presigned;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'mail'], function(Router $api) {
        $api->post('create', 'App\\Api\\V1\\Controllers\\MailController@create');
    });
    $api->group([
        'prefix' => 'mailgun',
        'middleware' => 'mailgun.webhook'
    ], function(Router $api) {
        $api->post('webhook/{id}', 'App\\Api\\V1\\Controllers\\MailgunController@store');
    });

    $api->group(['prefix' => 'webhooks'], function(Router $api) {
        $api->post('record/create/{id}', 'App\\Api\\V1\\Controllers\\RecordsController@createFromHook');
    });

    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\UsersController@me');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->group(['middleware' => 'admin'], function(Router $api) {
      // manage roles
      $api->get('roles', 'App\\Api\\V1\\Controllers\\RolesController@index');
      $api->get('roles/{id}', 'App\\Api\\V1\\Controllers\\RolesController@show');
      $api->post('roles', 'App\\Api\\V1\\Controllers\\RolesController@store');
      $api->post('roles/{id}', 'App\\Api\\V1\\Controllers\\RolesController@update');
      $api->delete('roles/{id}', 'App\\Api\\V1\\Controllers\\RolesController@destroy');
      // manage permissions
      $api->get('permissions', 'App\\Api\\V1\\Controllers\\PermissionsController@index');
      $api->get('permissions/{id}', 'App\\Api\\V1\\Controllers\\PermissionsController@show');
      $api->post('permissions', 'App\\Api\\V1\\Controllers\\PermissionsController@store');
      $api->post('permissions/{id}', 'App\\Api\\V1\\Controllers\\PermissionsController@update');
      $api->delete('permissions/{id}', 'App\\Api\\V1\\Controllers\\PermissionsController@destroy');

    });

    $api->group(['middleware' => 'bindings'], function(Router $api) {
      $api->post('/broadcasting/auth', function() {
          return response(request());
          Broadcast::auth(request());
      });

      $api->get('collections', 'App\\Api\\V1\\Controllers\\CollectionsController@index');
      $api->get('collections/{id}', 'App\\Api\\V1\\Controllers\\CollectionsController@show');
      $api->post('collections', 'App\\Api\\V1\\Controllers\\CollectionsController@store');
      $api->post('collections/attach/{id}', 'App\\Api\\V1\\Controllers\\CollectionsController@attach');
      $api->post('collections/detach/{id}', 'App\\Api\\V1\\Controllers\\CollectionsController@detach');
      $api->post('collections/{id}', 'App\\Api\\V1\\Controllers\\CollectionsController@update');
      $api->delete('collections/{id}', 'App\\Api\\V1\\Controllers\\CollectionsController@destroy');

      $api->get('document_types', 'App\\Api\\V1\\Controllers\\DocumentTypesController@index');
      $api->get('document_types/{id}', 'App\\Api\\V1\\Controllers\\DocumentTypesController@show');
      $api->post('document_types', 'App\\Api\\V1\\Controllers\\DocumentTypesController@store');
      $api->post('document_types/{id}', 'App\\Api\\V1\\Controllers\\DocumentTypesController@update');
      $api->delete('document_types/{id}', 'App\\Api\\V1\\Controllers\\DocumentTypesController@destroy');

      $api->get('documents', 'App\\Api\\V1\\Controllers\\DocumentsController@index');
      $api->get('documents/pending', 'App\\Api\\V1\\Controllers\\DocumentsController@pending');
      $api->get('documents/{id}', 'App\\Api\\V1\\Controllers\\DocumentsController@show');
      $api->post('documents', 'App\\Api\\V1\\Controllers\\DocumentsController@store');
      $api->post('documents/{id}', 'App\\Api\\V1\\Controllers\\DocumentsController@update');
      $api->delete('documents/{id}', 'App\\Api\\V1\\Controllers\\DocumentsController@destroy');

      $api->get('record_types', 'App\\Api\\V1\\Controllers\\RecordTypesController@index');
      $api->get('record_types/{id}', 'App\\Api\\V1\\Controllers\\RecordTypesController@show');
      $api->post('record_types', 'App\\Api\\V1\\Controllers\\RecordTypesController@store');
      $api->post('record_types/{id}', 'App\\Api\\V1\\Controllers\\RecordTypesController@update');
      $api->delete('record_types/{id}', 'App\\Api\\V1\\Controllers\\RecordTypesController@destroy');

      $api->get('records', 'App\\Api\\V1\\Controllers\\RecordsController@index');
      $api->get('records/{id}', 'App\\Api\\V1\\Controllers\\RecordsController@show');
      $api->post('records', 'App\\Api\\V1\\Controllers\\RecordsController@store');
      $api->post('records/{id}', 'App\\Api\\V1\\Controllers\\RecordsController@update');
      $api->delete('records/{id}', 'App\\Api\\V1\\Controllers\\RecordsController@destroy');

      $api->get('users', 'App\\Api\\V1\\Controllers\\UsersController@index');
      $api->get('users/{id}', 'App\\Api\\V1\\Controllers\\UsersController@show');
      $api->post('users', 'App\\Api\\V1\\Controllers\\UsersController@store');
      $api->post('users/{id}', 'App\\Api\\V1\\Controllers\\UsersController@update');
      $api->delete('users/{id}', 'App\\Api\\V1\\Controllers\\UsersController@destroy');

      $api->get('comments/{collection}', 'App\\Api\\V1\\Controllers\\CommentsController@index');
      $api->get('comments/{id}', 'App\\Api\\V1\\Controllers\\CommentsController@show');
      $api->post('comments/{collection}', 'App\\Api\\V1\\Controllers\\CommentsController@store');
      $api->post('comments/{id}', 'App\\Api\\V1\\Controllers\\CommentsController@update');
      $api->delete('comments/{id}', 'App\\Api\\V1\\Controllers\\CommentsController@destroy');

      $api->get('workflows', 'App\\Api\\V1\\Controllers\\WorkflowsController@index');
      $api->get('workflows/{id}', 'App\\Api\\V1\\Controllers\\WorkflowsController@show');
      $api->post('workflows', 'App\\Api\\V1\\Controllers\\WorkflowsController@store');
      $api->post('workflows/{id}', 'App\\Api\\V1\\Controllers\\WorkflowsController@update');
      $api->delete('workflows/{id}', 'App\\Api\\V1\\Controllers\\WorkflowsController@destroy');
      $api->get('workflows/publish/{id}', 'App\\Api\\V1\\Controllers\\WorkflowsController@publish');
      $api->get('workflows/unpublish/{id}', 'App\\Api\\V1\\Controllers\\WorkflowsController@unpublish');
      $api->get('workflows/duplicate/{id}', 'App\\Api\\V1\\Controllers\\WorkflowsController@duplicate');

      $api->get('views', 'App\\Api\\V1\\Controllers\\ViewsController@index');
      $api->get('views/{id}', 'App\\Api\\V1\\Controllers\\ViewsController@show');
      $api->post('views', 'App\\Api\\V1\\Controllers\\ViewsController@store');
      $api->post('views/{id}', 'App\\Api\\V1\\Controllers\\ViewsController@update');
      $api->delete('views/{id}', 'App\\Api\\V1\\Controllers\\ViewsController@destroy');
      $api->get('views/duplicate/{id}', 'App\\Api\\V1\\Controllers\\ViewsController@duplicate');

      $api->post('snooze', 'App\\Api\\V1\\Controllers\\SnoozeController@store');
      $api->post('s3-sign', function() {
          $input = request()->all();
          $s3 = S3Presigned::setPrefix('uploads/');
          $signedUrl = $s3->getSimpleUploadUrl(
              $input['filename'],
              10,
              [
                  'ContentType' => $input['content_type'],
                  'ContentDisposition' => 'inline'
              ]
          );
          return response()->json([
              'url' => $signedUrl,
              'method' => 'PUT'
          ]);
      });
    });
    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
});
