<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Camunda\Entity\Request\DeploymentRequest;
use App\Camunda\Service\DeploymentService;
use App\Camunda\Helper\FileCollection;
use App\Api\V1\Requests\WorkflowCreateRequest;
use App\Api\V1\Requests\WorkflowUpdateRequest;
use App\Repositories\WorkflowRepository;
use App\Validators\WorkflowValidator;
use App\Http\Controllers\Controller;
use App\Entities\Workflow;
use App\Events;

/**
 * Class WorkflowsController.
 *
 * @package namespace App\Api\V1\Controllers;
 */
class WorkflowsController extends Controller
{
    /**
     * @var WorkflowRepository
     */
    protected $repository;

    /**
     * @var WorkflowValidator
     */
    protected $validator;

    /**
     * WorkflowsController constructor.
     *
     * @param WorkflowRepository $repository
     * @param WorkflowValidator $validator
     */
    public function __construct(WorkflowRepository $repository, WorkflowValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = new DeploymentService(config('services.camunda.host'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $workflows = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json($workflows);
        }

        return view('workflows.index', compact('workflows'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  WorkflowCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(WorkflowCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $workflow = $this->repository->create($request->all());

            $response = $workflow->toArray();

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workflow = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json($workflow);
        }

        return view('workflows.show', compact('workflow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workflow = $this->repository->find($id);

        return view('workflows.edit', compact('workflow'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  WorkflowUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(WorkflowUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $workflow = $this->repository->update($request->all(), $id);

            $response = $workflow->toArray();

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workflow = $this->repository->find($id);
        $deleted = $this->repository->delete($id);
        if($workflow->published) {
            $res = $this->service->deleteById($workflow->bpmn_id);
        }

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Workflow deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Workflow deleted.');
    }

    /**
     * Publish the specified resource in storage.
     *
     * @param  WorkflowUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function publish(WorkflowUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $workflow = $this->repository->find($id);
            $files = new FileCollection();
            $files->addFile($workflow->name, $workflow->bpmn_xml, "$workflow->name.bpmn");
            $dep = new DeploymentRequest();
            $dep->set("deployment-name", $workflow->name);
            $dep->set("enable-duplicate-filtering", true);
            $dep->set("deploy-changed-only", true);
            $dep->set("deployment-source", "api");
            $dep->set("deployment-name", $workflow->name);
            $dep->set('files', $files);
            $res = $this->service->create($dep);
            $workflow->deployment_id = $res->id;
            $workflow->published = true;
            $workflow->save();

            $response = $workflow->toArray();

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Unpublish the specified resource in storage.
     *
     * @param  WorkflowUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function unpublish(WorkflowUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $workflow = $this->repository->find($id);
            $res = $this->service->deleteById($workflow->deployment_id);
            $workflow->published = false;
            $workflow->deployment_id = null;
            $workflow->save();

            $response = $workflow->toArray();

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Duplicate the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function duplicate($id)
    {
        $workflow = $this->repository->find($id);
        $newWorkflow = new Workflow();
        $newWorkflow->name = "Clone of " . $workflow->name;
        $newWorkflow->bpmn_id = $workflow->bpmn_id . '_clone';
        $newWorkflow->options = $workflow->options;
        $newWorkflow->document_type_id = $workflow->document_type_id;
        $newXml = str_replace($workflow->name, $newWorkflow->name, $workflow->bpmn_xml);
        $newWorkflow->bpmn_xml = str_replace($workflow->bpmn_id, $newWorkflow->bpmn_id, $newXml);
        $newWorkflow->save();

        if (request()->wantsJson()) {

            return response()->json($newWorkflow);
        }

        return view('workflows.show', compact('workflow'));
    }
}
