<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\MailHook;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    /**
     * Log the user in
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $input = $request->all();
        if ($input['email'] && $input['message'] && $input['subject']) {
            try {
                Mail::to($input['email'])
                ->queue(new MailHook($input['message'], $input['subject']));
            return response()
                ->json([
                    'status' => 'ok',
                    'message' => 'Message sent successfully'
                ]);
            } catch (Exception $e) {
                throw new HttpException(500, $e);
            }
        } else {
            return response([
                'error' => true,
                'message' => 'Invalid parameters in request'
            ], 500);

        }

    }
}
