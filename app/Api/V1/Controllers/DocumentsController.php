<?php

namespace App\Api\V1\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Entities\DocumentType;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Api\V1\Requests\DocumentCreateRequest;
use App\Api\V1\Requests\DocumentUpdateRequest;
use App\Repositories\DocumentRepository;
use App\Validators\DocumentValidator;
use App\Http\Controllers\Controller;
use App\Repositories\DocumentTypeRepository;
use MediaUploader;
use App\Entities\Media;
use App\Events\MediaCreated;

/**
 * Class DocumentsController.
 *
 * @package namespace App\Api\V1\Controllers;
 */
class DocumentsController extends Controller
{

    /**
     * @var DocumentValidator
     */
    protected $validator;
    protected $repository;

    /**
     * DocumentsController constructor.
     *
     * @param DocumentRepository $repository
     * @param DocumentValidator $validator
     */
    public function __construct(DocumentRepository $repository, DocumentValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $documents = $this->repository->has('document_type')->with(['document_type'])->all();
        $resp = $documents->map(function($media){
            $newMedia = $media->toArray();
            $newMedia['url'] = $media->getUrl();
            return $newMedia;
        });

        if (request()->wantsJson()) {

            return response()->json($resp);
        }

        return view('documents.index', compact('documents'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pending()
    {
        $medias = Media::inOrUnderDirectory('s3', 'uploads')->get();
        $mediasArray = $medias->map(function($media){
            $newMedia = $media->toArray();
            $newMedia['url'] = $media->getUrl();
            return $newMedia;
        });

        if (request()->wantsJson()) {

            return response()->json($mediasArray);
        }

        return view('documents.index', compact('documents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DocumentCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(DocumentCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $medias = $request->get('files');
            $mediaCollection = new Collection();
            foreach($medias as $media) {
                $document = MediaUploader::importPath('s3', $media['path']);
                if ($media['document_type_id']) {
                    $document->document_type_id = $media['document_type_id'];
                    $document->move("documents/{$document->document_type_id}");
                    event(new MediaCreated($document));
                }
                $mediaCollection->push($document);
            }

            $response = $mediaCollection;

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if (request()->query('pending')) {
            $media = Media::inOrUnderDirectory('s3', 'uploads')
                ->where('id', $id)
                ->get()
                ->first();
            $resp = $media->toArray();
            $resp['url'] = $media->getUrl();

        } else {
            $document = $this->repository->find($id);
            $resp = $document->toArray();
            $resp['url'] = $document->getUrl();
        }

        return response()->json($resp);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = $this->repository->find($id);

        return view('documents.edit', compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DocumentUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, DocumentUpdateRequest $request)
    {
        try {
            $input = $request->all();
            $document = Media::find($id);
            $document->document_type_id = $input['document_type_id'];
            $document->move("documents/{$document->document_type_id}");
            $resp = $document->toArray();
            $resp['url'] = $document->getUrl();
            return response()->json([$resp]);

        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (request()->query('pending')) {
            $document = Media::inOrUnderDirectory('s3', 'uploads')
                ->where('id', $id)
                ->get()
                ->first()
                ->delete();
        } else {
            $document = Media::destroy($id);
        }

        if (request()->wantsJson()) {

            return response()->json($document);
        }
    }
}
