<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Api\V1\Requests\DocumentTypeCreateRequest;
use App\Api\V1\Requests\DocumentTypeUpdateRequest;
use App\Repositories\DocumentTypeRepository;
use App\Validators\DocumentTypeValidator;
use App\Http\Controllers\Controller;

/**
 * Class DocumentTypesController.
 *
 * @package namespace App\Api\V1\Controllers;
 */
class DocumentTypesController extends Controller
{
    /**
     * @var DocumentTypeRepository
     */
    protected $repository;

    /**
     * @var DocumentTypeValidator
     */
    protected $validator;

    /**
     * DocumentTypesController constructor.
     *
     * @param DocumentTypeRepository $repository
     * @param DocumentTypeValidator $validator
     */
    public function __construct(DocumentTypeRepository $repository, DocumentTypeValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $documentTypes = $this->repository->all();

        return response()->json($documentTypes);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DocumentTypeCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(DocumentTypeCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $documentType = $this->repository->create($request->all());

            $response = $documentType->toArray();

            return response()->json($response);

        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $documentType = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json($documentType);
        }

        return view('types.show', compact('type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $documentType = $this->repository->find($id);

        return view('types.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DocumentTypeUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(DocumentTypeUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $documentType = $this->repository->update($request->all(), $id);

            $response = $documentType->toArray();

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json($deleted);
        }

        return redirect()->back()->with('message', 'DocumentType deleted.');
    }
}
