<?php

namespace App\Presenters;

use App\Transformers\ViewTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ViewPresenter.
 *
 * @package namespace App\Presenters;
 */
class ViewPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ViewTransformer();
    }
}
