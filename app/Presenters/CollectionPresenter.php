<?php

namespace App\Presenters;

use App\Transformers\CollectionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CollectionPresenter.
 *
 * @package namespace App\Presenters;
 */
class CollectionPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CollectionTransformer();
    }
}
