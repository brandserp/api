<?php

namespace App\Presenters;

use App\Transformers\RecordTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RecordTypePresenter.
 *
 * @package namespace App\Presenters;
 */
class RecordTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RecordTypeTransformer();
    }
}
