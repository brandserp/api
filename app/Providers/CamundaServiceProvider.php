<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \org\camunda\php\sdk;

class CamundaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->singleton('camunda', function($app) {
        // Initialize our API client which will be referenced by client scripts
        return new sdk\Api(config('services.camunda.host'));
      });
    }
}
