<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\CollectionRepository::class, \App\Repositories\CollectionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DocumentTypeRepository::class, \App\Repositories\DocumentTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CommentRepository::class, \App\Repositories\CommentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RoleRepository::class, \App\Repositories\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PermissionRepository::class, \App\Repositories\PermissionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\WorkflowRepository::class, \App\Repositories\WorkflowRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DocumentRepository::class, \App\Repositories\DocumentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RecordTypeRepository::class, \App\Repositories\RecordTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RecordRepository::class, \App\Repositories\RecordRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProcessRepository::class, \App\Repositories\ProcessRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ViewRepository::class, \App\Repositories\ViewRepositoryEloquent::class);
        //:end-bindings:
    }
}
