<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailHook extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $text, $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($text, $subject)
    {
        $this->text = $text;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->markdown('emails.mailhook');
    }
}
