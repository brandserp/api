<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RecordTypeRepository;
use App\Entities\RecordType;
use App\Validators\RecordTypeValidator;

/**
 * Class RecordTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RecordTypeRepositoryEloquent extends BaseRepository implements RecordTypeRepository
{
    protected $fieldSearchable = [
    	'id',
    	'name'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RecordType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RecordTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
