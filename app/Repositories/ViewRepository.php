<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ViewRepository.
 *
 * @package namespace App\Repositories;
 */
interface ViewRepository extends RepositoryInterface
{
    //
}
