<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CollectionRepository;
use App\Entities\Collection;
use App\Validators\CollectionValidator;

/**
 * Class CollectionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CollectionRepositoryEloquent extends BaseRepository implements CollectionRepository
{
    protected $fieldSearchable = [
    	'id',
    	'name'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Collection::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CollectionValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
