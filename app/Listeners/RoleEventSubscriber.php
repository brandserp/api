<?php

namespace App\Listeners;

use App\Camunda\Entity\Request\GroupRequest;
use App\Camunda\Service\GroupService;

class RoleEventSubscriber
{
    protected $service;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->service = new GroupService(config('services.camunda.host'));
    }

    /**
    * Handle user create events.
    */
    public function onRoleCreate($event) {
        $role = $event->role;
        $req = new GroupRequest();
        $req->set('id', $role->id);
        $req->set('name', $role->name);
        $result = $this->service->create($req);
    }

    /**
    * Handle user update events.
    */
    public function onRoleUpdate($event) {
        $role = $event->role;
        $req = new GroupRequest();
        $req->set('name', $role->name);
        $result = $this->service->update($role->id, $req);
    }

    /**
    * Handle user delete events.
    */
    public function onRoleDelete($event) {
        $this->service->delete($event->roleId);
    }

    /**
    * Register the listeners for the subscriber.
    *
    * @param  \Illuminate\Events\Dispatcher  $events
    */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\RoleCreated',
            'App\Listeners\RoleEventSubscriber@onRoleCreate'
        );
        $events->listen(
            'App\Events\RoleUpdated',
            'App\Listeners\RoleEventSubscriber@onRoleUpdate'
        );
        $events->listen(
            'App\Events\RoleDeleted',
            'App\Listeners\RoleEventSubscriber@onRoleDelete'
        );
    }
}
