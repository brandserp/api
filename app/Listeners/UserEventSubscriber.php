<?php

namespace App\Listeners;

use org\camunda\php\sdk;

class UserEventSubscriber
{
    protected $camunda;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->camunda = resolve('camunda');
    }

    /**
    * Handle user create events.
    */
    public function onUserCreate($event) {
        $user = $event->user;
        $cred = new sdk\entity\request\CredentialsRequest();
        $cred = $cred->setPassword($user->password);
        $profile = $profile = new sdk\entity\request\ProfileRequest();
        $profile = $profile->setId($user->id);
        $profile = $profile->setEmail($user->email);
        $profile = $profile->setFirstName($user->first_name);
        $profile = $profile->setLastName($user->last_name);
        $req = new sdk\entity\request\UserRequest();
        $req = $req->setCredentials($cred);
        $req = $req->setProfile($profile);
        $result = $this->camunda->user->createUser($req);
    }

    /**
    * Handle user update events.
    */
    public function onUserUpdate($event) {
        $user = $event->user;
        $cred = new sdk\entity\request\CredentialsRequest();
        $cred = $cred->setPassword($user->password);
        $profile = $profile = new sdk\entity\request\ProfileRequest();
        $profile = $profile->setId($user->id);
        $profile = $profile->setEmail($user->email);
        $profile = $profile->setFirstName($user->first_name);
        $profile = $profile->setLastName($user->last_name);
        $result = $this->camunda->user->updateCredentials($user->id, $cred);
        $result = $this->camunda->user->updateProfile($user->id, $profile);
    }

    /**
    * Handle user delete events.
    */
    public function onUserDelete($event) {
        $this->camunda->user->deleteUser($event->userId);
    }

    /**
    * Register the listeners for the subscriber.
    *
    * @param  \Illuminate\Events\Dispatcher  $events
    */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\UserCreated',
            'App\Listeners\UserEventSubscriber@onUserCreate'
        );
        $events->listen(
            'App\Events\UserUpdated',
            'App\Listeners\UserEventSubscriber@onUserUpdate'
        );
        $events->listen(
            'App\Events\UserDeleted',
            'App\Listeners\UserEventSubscriber@onUserDelete'
        );
    }
}
