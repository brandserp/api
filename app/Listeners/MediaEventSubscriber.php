<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use App\Camunda\Entity\Request\ProcessDefinitionRequest;
use App\Camunda\Service\ProcessDefinitionService;
use App\Camunda\Helper\VariableCollection;
use App\Entities\Workflows;
use App\Entities\Process;

class MediaEventSubscriber implements ShouldQueue
{
    protected $service;

    /**
     * Create the event listener.
     *
     * @return void
     */
     public function __construct() {
         $this->service = new ProcessDefinitionService(config('services.camunda.host'));
     }

    /**
    * Handle media create events.
    */
    public function onMediaCreate($event) {
        $media = $event->media;
        $documentType = $media->document_type;
        $workflows = $documentType->load(['workflows' => function($query) {
            $query->where('options->start_instance_on_upload', 'true');
        }]);


        if ($documentType->workflows->count()) {
            $documentType->workflows->each(function($w, $k) use($media) {
                try {
                    $vars = new VariableCollection();
                    $vars->addVariable('media_id', $media->id);
                    $vars->addVariable('media_link', $media->getUrl());
                    $req = new ProcessDefinitionRequest();
                    $req->set('variables', $vars);
                    $res = $this->service->startInstanceByKey($w->bpmn_id, null, $req);
                    $proc = Process::create([
                        'workflow_id' => $w->id,
                        'process_instance_id' => $res->id
                    ]);
                    $proc->attachMedia($media->id, 'default');
                } catch (Exception $e) {
                    report($e);
                    return false;
                }
            });
        }
    }


    /**
    * Register the listeners for the subscriber.
    *
    * @param  \Illuminate\Events\Dispatcher  $events
    */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\MediaCreated',
            'App\Listeners\MediaEventSubscriber@onMediaCreate'
        );
    }
}
