<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\View;

/**
 * Class ViewTransformer.
 *
 * @package namespace App\Transformers;
 */
class ViewTransformer extends TransformerAbstract
{
    /**
     * Transform the View entity.
     *
     * @param \App\Entities\View $model
     *
     * @return array
     */
    public function transform(View $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
