<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Collection;

/**
 * Class CollectionTransformer.
 *
 * @package namespace App\Transformers;
 */
class CollectionTransformer extends TransformerAbstract
{
    /**
     * Transform the Collection entity.
     *
     * @param \App\Entities\Collection $model
     *
     * @return array
     */
    public function transform(Collection $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
