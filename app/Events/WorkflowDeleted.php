<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class WorkflowDeleted
{
    use SerializesModels;

    public $workflowId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($workflowId)
    {
        $this->workflowId = $workflowId;
    }
}
