<?php

namespace App\Events;

use App\Workflow;
use Illuminate\Queue\SerializesModels;

class WorkflowPublished
{
    use SerializesModels;

    public $workflow;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Workflow $workflow)
    {
        $this->workflow = $workflow;
    }
}
