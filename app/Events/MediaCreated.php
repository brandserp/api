<?php

namespace App\Events;

use App\Entities\Media;
use Illuminate\Queue\SerializesModels;

class MediaCreated
{
    use SerializesModels;

    public $media;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Media $media)
    {
        $this->media = $media;
    }
}
