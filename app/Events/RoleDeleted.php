<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class RoleDeleted
{
    use SerializesModels;

    public $roleId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($roleId)
    {
        $this->roleId = $roleId;
    }
}
