<?php

namespace App\Events;

use App\Entities\Role;
use Illuminate\Queue\SerializesModels;

class RoleUpdated
{
    use SerializesModels;

    public $role;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Role $role)
    {
        $this->role = $role;
    }
}
