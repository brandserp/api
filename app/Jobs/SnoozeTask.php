<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Entities\User;
use App\Entities\TaskType;
use App\Entities\Thread;
use App\Notifications\SnoozeTaskNotif;

class SnoozeTask implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $thread, $user, $taskType;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, TaskType $taskType, Thread $thread)
    {
      $this->user = $user;
      $this->taskType = $taskType;
      $this->thread = $thread;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $data = [
        'thread' => $this->thread->id,
        'task_type' => $this->taskType->name
      ];
      $this->user->notify(new SnoozeTaskNotif($data));
    }
}
