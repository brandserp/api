<?php

namespace App\Entities;

use Plank\Mediable\Media as MediaParent;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Traits\CausesActivity;
use Laravel\Scout\Searchable;

/**
 * Class Record.
 *
 * @package namespace App\Entities;
 */
class Media extends MediaParent implements Transformable
{
    use TransformableTrait;
    use LogsActivity;
    use CausesActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['document_type_id'];
    protected static $logOnlyDirty = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function document_type()
    {
        return $this->belongsTo('App\Entities\DocumentType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function processes()
    {
        return $this->models('App\Entities\Process');
    }

    public function getLogNameToUse(string $eventName = ''): string
    {
       return 'documents';
    }

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Was {$eventName}";
    }

}
