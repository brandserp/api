<?php

namespace App\Entities;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use BrianFaust\Commentable\Models\Comment as CommentBase;

/**
 * Class Comment.
 *
 * @package namespace App\Entities;
 */
class Comment extends CommentBase implements Transformable
{
    use TransformableTrait;

}
