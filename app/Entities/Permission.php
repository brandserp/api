<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Permission\Models\Permission as SpatiePermission;
use Spatie\Permission\Contracts\Permission as SpatieContract;
use Laravel\Scout\Searchable;

/**
 * Class Permission.
 *
 * @package namespace App\Entities;
 */
class Permission extends SpatiePermission implements Transformable, SpatieContract
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
