<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Laravel\Scout\Searchable;

/**
 * Class Record.
 *
 * @package namespace App\Entities;
 */
class Record extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['record_type_id', 'data'];

    protected $casts = [
        'data' => 'array',
        'record_type_id' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function record_type()
    {
        return $this->hasOne('App\Entities\RecordType');
    }

}
