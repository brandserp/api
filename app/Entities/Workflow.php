<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Activitylog\Traits\LogsActivity;
use Laravel\Scout\Searchable;

/**
 * Class Workflow.
 *
 * @package namespace App\Entities;
 */
class Workflow extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bpmn_xml', 'name', 'bpmn_id', 'document_type_id', 'options'
    ];

    protected $casts = [
        'options' => 'array'
    ];

    public function document_type()
    {
        return $this->belongsTo('App\Entities\DocumentType');
    }

    public function processes()
    {
        return $this->hasMany('App\Entities\Process');
    }

    public function getLogNameToUse(string $eventName = ''): string
    {
       return 'workflows';
    }

}
