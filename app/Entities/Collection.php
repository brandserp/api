<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Plank\Mediable\Mediable;
use BrianFaust\Commentable\Traits\HasComments;
use Spatie\Activitylog\Traits\LogsActivity;
use Laravel\Scout\Searchable;

/**
 * Class Collection.
 *
 * @package namespace App\Entities;
 */
class Collection extends Model implements Transformable
{
    use TransformableTrait;
    use Mediable;
    use HasComments;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
    protected static $logAttributes = ['name'];

    /**
     * The attributes that are searchable
     *
     * @var array
     */
    protected $searchable = [];


    public function getLogNameToUse(string $eventName = ''): string
    {
       return 'collections';
    }

}
