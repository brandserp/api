<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Laravel\Scout\Searchable;

/**
 * Class RecordType.
 *
 * @package namespace App\Entities;
 */
class RecordType extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'schema', 'ui_schema', 'meta'];

    protected $casts = [
        'schema' => 'array',
        'ui_schema' => 'array',
        'meta' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function records()
    {
        return $this->hasMany('App\Entities\Record');
    }
}
