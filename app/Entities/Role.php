<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Permission\Models\Role as SpatieRole;
use Spatie\Permission\Contracts\Role as SpatieContract;

/**
 * Class Role.
 *
 * @package namespace App\Entities;
 */
class Role extends SpatieRole implements Transformable, SpatieContract
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
}
