<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Activitylog\Traits\LogsActivity;
use Laravel\Scout\Searchable;

/**
 * Class DocumentType.
 *
 * @package namespace App\Entities;
 */
class DocumentType extends Model implements Transformable
{
    use TransformableTrait;
    use LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name', 'properties', 'description'
    ];

    protected $casts = [
        'properties' => 'array'
    ];

    protected $logAttributes = ['name', 'properties', 'description'];
    protected static $logOnlyDirty = true;


    public function processes()
    {
        return $this->hasManyThrough('App\Entities\Workflow', 'App\Entities\Process');
    }

    public function workflows()
    {
        return $this->hasMany('App\Entities\Workflow');
    }

    public function getLogNameToUse(string $eventName = ''): string
    {
       return 'document_types';
    }
}
